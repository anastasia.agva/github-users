import { User, Repo, SearchResults } from './types';
const gitHubToken = '';

const baseQueryUrl = 'https://api.github.com';

export const getUsersList = (): Promise<User[]> => {
  return fetch(`${baseQueryUrl}/users`, {
    method: 'get',
    headers: {
      Authorization: `token ${gitHubToken}`,
    },
  }).then((response) => response.json());
};

export const getSingleUser = (login: string): Promise<User> => {
  return fetch(`${baseQueryUrl}/users/${login}`, {
    method: 'get',
    headers: {
      Authorization: `token ${gitHubToken}`,
    },
  }).then((response) => response.json());
};

export const getRepos = (login: string): Promise<Repo[]> => {
  return fetch(`${baseQueryUrl}/users/${login}/repos`, {
    method: 'get',
    headers: {
      Authorization: `token ${gitHubToken}`,
    },
  }).then((response) => response.json());
};

export const getSearchResults = (searchQuery: string): Promise<SearchResults> => {
  return fetch(`${baseQueryUrl}/search/users?q=${searchQuery}`, {
    method: 'get',
    headers: {
      Authorization: `token ${gitHubToken}`,
    },
  }).then((response) => response.json());
};
