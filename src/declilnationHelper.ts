export const getNounEnding = (count: number, nounForms: string[]): string => {
  const divisionRemainder = count % 10;
  if (count > 10 && count < 20) {
    return nounForms[2];
  }
  if (divisionRemainder === 1) {
    return nounForms[0];
  }
  if (divisionRemainder > 1 && divisionRemainder < 5) {
    return nounForms[1];
  }
  return nounForms[2];
};
