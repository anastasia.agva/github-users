import React, { FC } from 'react';
import './UserProfilePage.css';
import { useParams } from 'react-router-dom';
import { User, Repo } from '../../types';
import { getRepos, getSingleUser } from '../../apiQueries';
import { getNounEnding } from '../../declilnationHelper';

export const UserProfilePage: FC = () => {
  const { login }: { login: string } = useParams();
  const [user, setUser] = React.useState<User | null>(null);
  const [repos, setRepos] = React.useState<Repo[]>([]);
  const followersEndingVariations = ['подписчик', 'подписчика', 'подписчиков'];
  const followingEndingVariations = ['подписка', 'подписки', 'подписок'];
  React.useEffect(() => {
    Promise.all([getSingleUser(login), getRepos(login)]).then((responses) => {
      setUser(responses[0].login ? responses[0] : null);
      setRepos(responses[1].length ? responses[1] : []);
    });
  }, []);

  return (
    <>
      {user ? (
        <div className="container">
          <section className="user-profile">
            <div className="user-profile__image-container">
              <img className="user-profile__image" src={user.avatar_url} alt={`${user.login} profile photo`} />
            </div>
            <div className="user-profile__content">
              <h1 className="user-profile__title">
                {user.name}, <span className="user-profile__accent">{user.login}</span>
              </h1>
              <p className="user-profile__text">
                <span className="user-profile__accent">{user.followers}</span>{' '}
                {getNounEnding(user.followers, followersEndingVariations)} ·{' '}
                <span className="user-profile__accent">{user.following}</span>{' '}
                {getNounEnding(user.following, followingEndingVariations)} ·{' '}
                <a href={user.blog} className="link" target="_blank">
                  {user.blog}
                </a>
              </p>
            </div>
          </section>

          <section className="repository-list">
            <div className="repository-list__header">
              <h2 className="repository-list__title">Репозитории</h2>
              <a href={`https://github.com/${user.login}?tab=repositories`} className="link" target="_blank">
                Все репозитории
              </a>
            </div>

            <div className="repository-list__container">
              {repos.map((item) => (
                <section className="repository-list__item" key={item.id}>
                  <h3 className="repository-list__item-title">
                    <a href={`https://github.com/${item.owner.login}/${item.name}`} className="link" target="_blank">
                      {item.name}
                    </a>
                  </h3>
                  <p className="repository-list__item-text">{item.description}</p>
                </section>
              ))}
            </div>
          </section>
        </div>
      ) : null}
    </>
  );
};
