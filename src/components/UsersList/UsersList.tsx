import React, { FC } from 'react';
import './UsersList.css';
import { Link } from 'react-router-dom';
import { User } from '../../types';
import { getNounEnding } from '../../declilnationHelper';

interface Props {
  users: User[];
}

export const UsersList: FC<Props> = ({ users }) => {
  const reposEndingVariations = ['репозиторий', 'репозитория', 'репозиториев'];
  return (
    <div className="users-list">
      {users.map((item) => (
        <section className="users-list__item" key={item.id}>
          <div className="users-list__image-container">
            <img className="users-list__image" src={item.avatar_url} alt={`${item.login} profile photo`} />
          </div>
          {}
          <div className="users-list__content">
            <h2 className="users-list__title">
              <Link to={`/users/${item.login}`} className="link">
                {item.login}
              </Link>
              , {item.public_repos} {getNounEnding(item.public_repos, reposEndingVariations)}
            </h2>
            {item.company && <p className="users-list__text">{item.company}</p>}
          </div>
        </section>
      ))}
    </div>
  );
};
