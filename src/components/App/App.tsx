import React, { FC, useEffect } from 'react';
import { Switch, Route, useLocation, Redirect } from 'react-router-dom';
import { UserProfilePage } from '../UserProfilePage/UserProfilePage';
import { UsersPage } from '../UsersPage/UsersPage';
import { Header } from '../Header/Header';
import { UsersSearchPage } from '../UsersSearchPage/UsersSearchPage';

export const App: FC = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);
  return (
    <>
      <Header />
      <main>
        <Switch>
          <Route path="/search">
            <UsersSearchPage></UsersSearchPage>
          </Route>
          <Route path="/users/:login">
            <UserProfilePage />
          </Route>
          <Route path="/users">
            <UsersPage />
          </Route>
          <Route path="/">
            <UsersPage />
          </Route>
          <Route>
            <Redirect to="/" />
          </Route>
        </Switch>
      </main>
    </>
  );
};
