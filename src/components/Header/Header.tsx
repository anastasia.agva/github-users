import React, { FC, FormEvent, useState } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import './Header.css';

export const Header: FC = () => {
  const [searchValue, setSearchValue] = useState('');
  const history = useHistory();
  const userPageMatch = useRouteMatch('/users/:login');
  const searchPageMatch = useRouteMatch('/search');
  const [headerTitle, setHeaderTitle] = useState('');
  const onSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!searchValue.trim().length) {
      return;
    }
    history.push({ pathname: '/search', search: `?query=${searchValue}` });
    setSearchValue('');
  };

  React.useEffect(() => {
    if (searchPageMatch) {
      setHeaderTitle('поиск');
      return;
    }

    if (userPageMatch) {
      const userName = new URLSearchParams(userPageMatch?.params).get('login') ?? '';
      setHeaderTitle(userName);
      return;
    }

    setHeaderTitle('');
  });

  return (
    <header className="header">
      <div className="container header__container">
        <nav className="header__navigation">
          <ul className="header__navigation-list">
            <li className="header__navigation-list-item">
              <a href="/" className="header__navigation-link">
                Пользователи гитхаба
              </a>
            </li>
            {headerTitle ? (
              <li className="header__navigation-list-item">
                <a className="header__navigation-link header__navigation-link--user">{headerTitle}</a>
              </li>
            ) : null}
          </ul>
        </nav>

        <div className="header__search">
          <form className="header__search-form" onSubmit={onSubmit}>
            <input
              type="search"
              className="header__search-input"
              placeholder="Поиск пользователя"
              value={searchValue}
              onChange={(event) => setSearchValue(event.currentTarget.value)}
            />
            <button type="submit" className="header__search-button">
              Найти
            </button>
          </form>
        </div>
      </div>
    </header>
  );
};
