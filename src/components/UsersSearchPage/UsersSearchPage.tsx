import React, { FC } from 'react';
import { UsersList } from '../UsersList/UsersList';
import { User } from '../../types';
import { getSearchResults, getSingleUser } from '../../apiQueries';
import { useLocation } from 'react-router-dom';

export const UsersSearchPage: FC = () => {
  const [users, setUsers] = React.useState<User[]>([]);
  const searchQueryUrl = useLocation();
  const searchQuery = new URLSearchParams(searchQueryUrl?.search).get('query') ?? '';

  React.useEffect(() => {
    getSearchResults(searchQuery)
      .then((response) => response.items)
      .then((response) => {
        return Promise.all(response.map((item) => getSingleUser(item.login)));
      })
      .then((response) => setUsers(response));
  }, [searchQuery]);

  return (
    <>
      <main>
        <div className="container">
          {users.length ? (
            <>
              <h1 className="title">Пользователи по запросу {searchQuery}</h1>
              <UsersList users={users} />
            </>
          ) : (
            <h1 className="title">Ничего не найдено по запросу {searchQuery}</h1>
          )}
        </div>
      </main>
    </>
  );
};
