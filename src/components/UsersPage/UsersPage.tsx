import React, { FC } from 'react';
import { UsersList } from '../UsersList/UsersList';
import { Link } from 'react-router-dom';
import { User } from '../../types';
import { getSingleUser, getUsersList } from '../../apiQueries';

export const UsersPage: FC = () => {
  const [users, setUsers] = React.useState<User[]>([]);
  React.useEffect(() => {
    getUsersList()
      .then((response) => {
        return Promise.all(response.map((item) => getSingleUser(item.login)));
      })
      .then((response) => setUsers(response));
  }, []);

  return (
    <Link to="/">
      <>
        <main>
          <div className="container">
            <UsersList users={users} />
          </div>
        </main>
      </>
    </Link>
  );
};
